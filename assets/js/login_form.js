jQuery(document).ready(function($){
	jQuery('.toggle').on('click', function() {
  jQuery('.container').stop().addClass('active');
});

jQuery('.close').on('click', function() {
  jQuery('.container').stop().removeClass('active');
});
   
   jQuery('#bulk_import_but').click(function() {
        jQuery('#bulk_import_but').find('span').toggleClass("active");
        alert("active");
   });
  
   jQuery('#addnew_but').click(function() {

   setTimeout(function(){
      var popup_ht = jQuery('.popup_left_div').height();
   jQuery('.popup_right_div').css('height',popup_ht);
   
   
    var map;

    var style = [
		{
		stylers: [
			{ color: '#333333' }
		]
		},
		{elementType: 'geometry', stylers: [{color: '#333333'}]},
		{elementType: 'labels.text.fill', stylers: [{color: '#676767'}]},
		{elementType: 'labels.text.stroke', stylers: [{color: '#232323'}]},{
		featureType: "poi",
		stylers: [
			{ color: "#333333"}
		]
		},{
		featureType: "transit",
		stylers: [
			{ color: "#fff" }
		]
		},{
		featureType: "road",
		stylers: [
			{ color: "red" }
		]
		},{
		 featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{color: '#737373'}]
              },
		{
		featureType: "landscape",
		stylers: [
			{ color: "#333333" }
		]
		},{
		featureType: 'water',
        stylers: [{color: '#215367'}]
            },
	]

    var options = {
        zoom: 4,
        center:  new google.maps.LatLng(45.50867, -73.553992),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: false,
		 mapTypeControlOptions: {
      position: google.maps.ControlPosition.BOTTOM_CENTER,
    },
	    zoomControl: true,
          zoomControlOptions: {
              position: google.maps.ControlPosition.RIGHT_CENTER
          },
        streetViewControl: false,
    };
    
   map = new google.maps.Map(jQuery('#map2')[0], options);
    map.setOptions({
        styles: style,
		minZoom: 2, maxZoom: 15
    });
   
   
   
   
}, 300);
  });
   jQuery('.glyphicon-resize-small').closest("li").css('display','none');
   jQuery('.glyphicon-fullscreen').click(function() {
    jQuery('#fullscreen').fullscreen();
    return false;
  });

jQuery('.glyphicon-resize-small').click(function() {
    jQuery.fullscreen.exit();
    return false;
  });
  
  jQuery(document).bind('fscreenchange', function(e, state, elem) {
    // if we currently in fullscreen mode
    if (jQuery.fullscreen.isFullScreen()) {
      jQuery('.glyphicon-resize-small').closest("li").css('display','inline-block');
      jQuery('.glyphicon-fullscreen').closest("li").css('display','none');
    } else {
      jQuery('.glyphicon-fullscreen').closest("li").css('display','inline-block');
      jQuery('.glyphicon-resize-small').closest("li").css('display','none');
    }
    jQuery('#state').text(jQuery.fullscreen.isFullScreen() ? '' : 'not');
  });

    jQuery('.glyphicon-volume-up').click(function(){
    jQuery(this).toggleClass('glyphicon-volume-up').toggleClass('glyphicon-volume-off');
	
});

    jQuery(".width_3").click(function(){
	 jQuery(".width_3").removeClass("active");
	   jQuery(this).addClass("active");
	});
	jQuery(".width_3_rt").click(function(){
	 jQuery(".width_3_rt").removeClass("active");
	   jQuery(this).addClass("active");
	});
    jQuery(".left_marker").click(function(){
	  var k = jQuery('.left-window').width();
	  var y = jQuery(window).width();
	   var result = y-k;
	  if(jQuery(".left-window").hasClass('open')){
	    jQuery(".left-window").animate({left: -k});
		jQuery(".left-window").removeClass('open');
		jQuery(".icn_left").addClass('rotate_pointer');
		jQuery('#map>div').animate({marginLeft: '0%'});
	  }
      else{
	    jQuery(".left-window").animate({left: '0'});
		jQuery(".left-window").addClass('open');
		jQuery(".icn_left").removeClass('rotate_pointer');
		jQuery('#map>div').animate({marginLeft: k});
	  }
    });
	 jQuery(".right_marker").click(function(){
	  var k = jQuery('.left-window').width();
	  var y = jQuery(window).width();

	  var result = y-k;
	  if(jQuery(".right-window").hasClass('open')){
	    jQuery(".right-window").animate({right: -k});
		jQuery(".right-window").removeClass('open');
		jQuery(".icn_right").addClass('rotate_pointer');
		jQuery('#map>div').animate({width: '100%'});

	  }
      else{
	    jQuery(".right-window").animate({right: '0'});
		jQuery(".right-window").addClass('open');
		jQuery(".icn_right").removeClass('rotate_pointer');
			  	  jQuery('#map>div').animate({width: result});
	  }
    });



	
	
	jQuery(document).ready(function () {
    var map;

    var style = [
		{
		stylers: [
			{ color: '#333333' }
		]
		},
		{elementType: 'geometry', stylers: [{color: '#333333'}]},
		{elementType: 'labels.text.fill', stylers: [{color: '#676767'}]},
		{elementType: 'labels.text.stroke', stylers: [{color: '#232323'}]},{
		featureType: "poi",
		stylers: [
			{ color: "#333333"}
		]
		},{
		featureType: "transit",
		stylers: [
			{ color: "#fff" }
		]
		},{
		featureType: "road",
		stylers: [
			{ color: "red" }
		]
		},{
		 featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{color: '#737373'}]
              },
		{
		featureType: "landscape",
		stylers: [
			{ color: "#333333" }
		]
		},{
		featureType: 'water',
        stylers: [{color: '#215367'}]
            },
	]

    var options = {
        zoom: 3,
        center:  new google.maps.LatLng(45.50867, -73.553992),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: false,
		 mapTypeControlOptions: {
      position: google.maps.ControlPosition.BOTTOM_CENTER,
    },
	    zoomControl: true,
          zoomControlOptions: {
              position: google.maps.ControlPosition.RIGHT_CENTER
          },
        streetViewControl: false,
    };
    
    map = new google.maps.Map(jQuery('#map')[0], options);
    map.setOptions({
        styles: style,
		minZoom: 2, maxZoom: 15
    });
});


});
